package ru.example.form.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import ru.example.form.json.RegistrationRequest;
import ru.example.form.service.RegistrationFormServiceImpl;
import ru.example.form.service.dto.RegistrationFormDTO;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ru.example.form.service.dto.RegistrationAnswerEnum.*;

@WebMvcTest(RegistrationController.class)
@RunWith(SpringRunner.class)
class RegistrationControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    RegistrationFormServiceImpl service;


    @Test
    void registrationALREADY() throws Exception {

        RegistrationFormDTO dto = new RegistrationFormDTO()
                .setEmail("1")
                .setLogin("1")
                .setName("1")
                .setPassword("1");

        RegistrationRequest request = new RegistrationRequest();
        request.setEmail("1");
        request.setLogin("1");
        request.setName("1");
        request.setPassword("1");


        when(service.regist(dto)).thenReturn(ALREADY);
        mockMvc.perform(MockMvcRequestBuilders.post("/registration")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(request))
        )
                .andExpect(status().isBadRequest());
    }

    @Test
    void registrationWAIT() throws Exception {

        RegistrationFormDTO dto = new RegistrationFormDTO()
                .setEmail("1@mail.ru")
                .setLogin("1")
                .setName("1")
                .setPassword("1");

        RegistrationRequest request = new RegistrationRequest();
        request.setEmail("1@mail.ru");
        request.setLogin("1");
        request.setName("1");
        request.setPassword("1");

        when(service.regist(dto)).thenReturn(WAIT);
        mockMvc.perform(MockMvcRequestBuilders.post("/registration")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(request))
        )
                .andExpect(status().isOk());
    }

    @Test
    void registrationDONE() throws Exception {

        RegistrationFormDTO dto = new RegistrationFormDTO()
                .setEmail("1@mail.ru")
                .setLogin("1")
                .setName("1")
                .setPassword("1");

        RegistrationRequest request = new RegistrationRequest();
        request.setEmail("1@mail.ru");
        request.setLogin("1");
        request.setName("1");
        request.setPassword("1");

        when(service.regist(dto)).thenReturn(DONE);
        mockMvc.perform(MockMvcRequestBuilders.post("/registration")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(request))
        )
                .andExpect(status().isOk());
    }

    @Test
    void registrationCANCEL() throws Exception {

        RegistrationFormDTO dto = new RegistrationFormDTO()
                .setEmail("1@mail.ru")
                .setLogin("1")
                .setName("1")
                .setPassword("1");

        RegistrationRequest request = new RegistrationRequest();
        request.setEmail("1@mail.ru");
        request.setLogin("1");
        request.setName("1");
        request.setPassword("1");

        when(service.regist(dto)).thenReturn(CANCEL);
        mockMvc.perform(MockMvcRequestBuilders.post("/registration")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(request))
        )
                .andExpect(status().isOk());
    }
}