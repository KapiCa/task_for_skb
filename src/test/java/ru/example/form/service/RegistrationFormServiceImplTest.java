package ru.example.form.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import ru.example.form.entity.Confirm;
import ru.example.form.entity.RegistrationForm;
import ru.example.form.message.Message;
import ru.example.form.repository.ConfirmRepository;
import ru.example.form.repository.RegistrationRepository;
import ru.example.form.service.converter.RegistrationFormDTOToEntityConverter;
import ru.example.form.service.dto.RegistrationAnswerEnum;
import ru.example.form.service.dto.RegistrationFormDTO;

import java.util.List;
import java.util.concurrent.TimeoutException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@SpringBootTest
@RunWith(SpringRunner.class)
class RegistrationFormServiceImplTest {
    @Autowired
    RegistrationFormServiceImpl subj;

    @MockBean
    RegistrationRepository registrationRepository;
    @MockBean
    ConfirmRepository confirmRepository;
    @MockBean
    RegistrationFormDTOToEntityConverter registrationFormDTOToEntityConverter;
    @MockBean
    SendMailer sendMailer;
    @MockBean
    MessagingService messagingService;
    @MockBean
    SchedulerForConfirm schedulerForConfirm;


    @BeforeEach
    void setUp() {
    }

    @Test
    void registALREADY() {

        RegistrationFormDTO dto = new RegistrationFormDTO()
                .setEmail("1")
                .setLogin("1")
                .setName("1")
                .setPassword("1");

        RegistrationForm form = new RegistrationForm();
        form.setEmail("1")
                .setLogin("1")
                .setName("1")
                .setPassword("1")
                .setId(1);


        when(registrationRepository.findByEmail("1")).thenReturn(form);
        RegistrationAnswerEnum regist = subj.regist(dto);

        assertEquals(regist, RegistrationAnswerEnum.ALREADY);
    }

    @Test
    void registDONE() throws TimeoutException {

        RegistrationFormDTO dto = new RegistrationFormDTO()
                .setEmail("2")
                .setLogin("2")
                .setName("2")
                .setPassword("2");

        Message<Object> message = new Message<>();
        message.setFlag(true);
        when(messagingService.doRequest(new Message<>())).thenReturn(message);
        when(registrationRepository.findByEmail("2")).thenReturn(null);
        RegistrationAnswerEnum regist = subj.regist(dto);

        assertEquals(regist, RegistrationAnswerEnum.DONE);
    }

    @Test
    void registCANCEL() throws TimeoutException {

        RegistrationFormDTO dto = new RegistrationFormDTO()
                .setEmail("2")
                .setLogin("2")
                .setName("2")
                .setPassword("2");

        Message<Object> message = new Message<>();
        message.setFlag(false);
        when(messagingService.doRequest(new Message<>())).thenReturn(message);
        when(registrationRepository.findByEmail("2")).thenReturn(null);
        RegistrationAnswerEnum regist = subj.regist(dto);

        assertEquals(regist, RegistrationAnswerEnum.CANCEL);
    }

    @Test
    void registWAIT() throws TimeoutException {

        RegistrationFormDTO dto = new RegistrationFormDTO()
                .setEmail("2")
                .setLogin("2")
                .setName("2")
                .setPassword("2");

        RegistrationForm form = new RegistrationForm();
        form.setEmail("2")
                .setLogin("2")
                .setName("2")
                .setPassword("2")
                .setId(2);

        Confirm confirm = new Confirm();
        confirm.setId(1).setRegistrationForm(form);

        when(registrationRepository.findByEmail("2")).thenReturn(null);
        when(messagingService.doRequest(new Message<>())).thenThrow(TimeoutException.class);
        when(confirmRepository.save(new Confirm().setRegistrationForm(form))).thenReturn(confirm);
        RegistrationAnswerEnum regist = subj.regist(dto);

        List<Confirm> all = confirmRepository.findAll();
        for (Confirm con : all) {
            assertEquals(con,confirm);
        }
        assertEquals(regist, RegistrationAnswerEnum.WAIT);

    }

}