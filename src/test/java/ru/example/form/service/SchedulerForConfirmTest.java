package ru.example.form.service;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import ru.example.form.entity.Confirm;
import ru.example.form.entity.RegistrationForm;
import ru.example.form.message.Message;
import ru.example.form.repository.ConfirmRepository;

import java.util.List;
import java.util.concurrent.TimeoutException;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

@SpringBootTest
@RunWith(SpringRunner.class)
class SchedulerForConfirmTest {
    @Autowired
    SchedulerForConfirm subj;
    @MockBean
    ConfirmRepository confirmRepository;
    @MockBean
    MessagingService messagingService;
    @MockBean
    SendMailer sendMailer;

    @Test
    void confirmDONEWithMessageFlagIsTrue() throws TimeoutException {

        RegistrationForm form = new RegistrationForm();
        form.setEmail("1")
                .setLogin("1")
                .setName("1")
                .setPassword("1")
                .setId(1);

        Confirm confirm = new Confirm();
        confirm.setId(1)
                .setRegistrationForm(form);

        Message<Object> message = new Message<>();
        message.setFlag(true);

        when(confirmRepository.save(new Confirm().setRegistrationForm(form))).thenReturn(confirm);
        when(messagingService.doRequest(new Message<>())).thenReturn(message);
        subj.confirm();

        List<Confirm> all = confirmRepository.findAll();
        assertTrue(all.isEmpty());
    }

    @Test
    void confirmDONEWithMessageFlagIsFalse() throws TimeoutException {

        RegistrationForm form = new RegistrationForm();
        form.setEmail("1")
                .setLogin("1")
                .setName("1")
                .setPassword("1")
                .setId(1);

        Confirm confirm = new Confirm();
        confirm.setId(1)
                .setRegistrationForm(form);

        Message<Object> message = new Message<>();
        message.setFlag(false);

        when(confirmRepository.save(new Confirm().setRegistrationForm(form))).thenReturn(confirm);
        when(messagingService.doRequest(new Message<>())).thenReturn(message);
        subj.confirm();

        List<Confirm> all = confirmRepository.findAll();
        assertTrue(all.isEmpty());
    }
}