package ru.example.form.service.dto;

public enum RegistrationAnswerEnum {
    ALREADY,
    DONE,
    WAIT,
    CANCEL
}
