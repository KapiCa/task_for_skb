package ru.example.form.service.dto;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class RegistrationFormDTO {
    private String login;
    private String password;
    private String email;
    private String name;

}
