package ru.example.form.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.example.form.entity.Confirm;
import ru.example.form.entity.RegistrationForm;
import ru.example.form.message.Message;
import ru.example.form.repository.ConfirmRepository;
import ru.example.form.repository.RegistrationRepository;
import ru.example.form.service.converter.RegistrationFormDTOToEntityConverter;
import ru.example.form.service.dto.RegistrationFormDTO;
import ru.example.form.service.dto.RegistrationAnswerEnum;

import java.util.concurrent.TimeoutException;

import static ru.example.form.constants.Constans.BADANSWER;
import static ru.example.form.constants.Constans.GOODANSWER;

@Service
@Transactional
@RequiredArgsConstructor
public class RegistrationFormServiceImpl implements RegistrationFormService {
    private final RegistrationRepository registrationRepository;
    private final RegistrationFormDTOToEntityConverter registrationFormDTOToEntityConverter;
    private final SendMailer sendMailer;
    private final MessagingService messagingService;
    private final ConfirmRepository confirmRepository;


    public RegistrationAnswerEnum regist(RegistrationFormDTO registrationFormDTO) {
        if (registrationRepository.findByEmail(registrationFormDTO.getEmail()) == null) {

            RegistrationForm save = registrationRepository.save(registrationFormDTOToEntityConverter.convert(registrationFormDTO));

            try {
                Message<Object> message = messagingService.doRequest(new Message<>());

                if (message.isFlag()) {
                    sendMailer.sendMail(registrationFormDTO.getEmail(), GOODANSWER);
                    return RegistrationAnswerEnum.DONE;
                } else {
                    sendMailer.sendMail(registrationFormDTO.getEmail(), BADANSWER);
                    registrationRepository.delete(save);
                    return RegistrationAnswerEnum.CANCEL;
                }

            } catch (TimeoutException e) {
                Confirm confirm = new Confirm().setRegistrationForm(save);
                confirmRepository.save(confirm);
                return RegistrationAnswerEnum.WAIT;
            }

        }
        return RegistrationAnswerEnum.ALREADY;
    }
}
