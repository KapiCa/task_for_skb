package ru.example.form.service.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ru.example.form.entity.RegistrationForm;
import ru.example.form.service.dto.RegistrationFormDTO;

@Component
public class RegistrationFormDTOToEntityConverter implements Converter<RegistrationFormDTO, RegistrationForm> {
    @Override
    public RegistrationForm convert(RegistrationFormDTO registrationFormDTO) {
        return new RegistrationForm()
                .setEmail(registrationFormDTO.getEmail())
                .setPassword(registrationFormDTO.getPassword())
                .setName(registrationFormDTO.getName())
                .setLogin(registrationFormDTO.getLogin());
    }
}
