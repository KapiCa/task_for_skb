package ru.example.form.service;

import org.springframework.stereotype.Service;

import java.util.concurrent.TimeoutException;

public interface SendMailer {
    void sendMail (String toAddress, String messageBody) throws TimeoutException;
}
