package ru.example.form.service;

import ru.example.form.service.dto.RegistrationFormDTO;
import ru.example.form.service.dto.RegistrationAnswerEnum;

import java.util.concurrent.TimeoutException;

public interface RegistrationFormService {
    RegistrationAnswerEnum regist(RegistrationFormDTO registrationFormDTO) throws TimeoutException;
}
