package ru.example.form.service;

import ru.example.form.message.Message;
import ru.example.form.message.MessageId;

import java.util.concurrent.TimeoutException;

public interface MessagingService {
    <T> MessageId send(Message<T> msg);
    <T> Message<T> receive(MessageId messageId) throws TimeoutException;
    <R, A> Message<A> doRequest(Message<R> request) throws TimeoutException;
}
