package ru.example.form.service;

import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

@Service
public class SendMailerStub implements SendMailer {

    @Autowired
    JavaMailSender javaMailSender;

    @Override
    public void sendMail(String toAddress, String messageBody) throws TimeoutException {
        Logger log = LoggerFactory.getLogger(Log4j.class);
        if (shouldThrowTimeout()) {
            sleep();

            throw new TimeoutException("Timeout!");
        }

        if (shouldSleep()) {
            sleep();
        }

        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setFrom("mail");
        mailMessage.setTo(toAddress);
        mailMessage.setSubject("form Confirm");
        mailMessage.setText(messageBody);

        javaMailSender.send(mailMessage);

        log.info("Message sent to {" + toAddress + "} body {" + messageBody + "} .");
    }

    @SneakyThrows
    private static void sleep() {
        Thread.sleep(TimeUnit.MINUTES.toMillis(1));
    }

    private static boolean shouldSleep() {
        return new Random().nextInt(10) == 1;
    }

    private static boolean shouldThrowTimeout() {
        return new Random().nextInt(10) == 1;
    }


}
