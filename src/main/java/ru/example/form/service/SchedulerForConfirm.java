package ru.example.form.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.example.form.entity.Confirm;
import ru.example.form.message.Message;
import ru.example.form.repository.ConfirmRepository;

import java.util.List;
import java.util.concurrent.TimeoutException;

import static ru.example.form.constants.Constans.BADANSWER;
import static ru.example.form.constants.Constans.GOODANSWER;

@Component
@Transactional(rollbackFor = TimeoutException.class)
@RequiredArgsConstructor
public class SchedulerForConfirm {
    private final ConfirmRepository confirmRepository;
    private final MessagingService messagingService;
    private final SendMailer sendMailer;


    @Scheduled(fixedRate = 30000)
    public void confirm() {
        Logger log = LoggerFactory.getLogger(Log4j.class);
        List<Confirm> all = confirmRepository.findAll();

        for (Confirm confirm : all) {
            try {
                Message<Object> message = messagingService.doRequest(new Message<>());
                if (message.isFlag()) {
                    sendMailer.sendMail(confirm.getRegistrationForm().getEmail(), GOODANSWER);
                    confirmRepository.delete(confirm);
                } else {
                    sendMailer.sendMail(confirm.getRegistrationForm().getEmail(), BADANSWER);
                    confirmRepository.delete(confirm);
                }
                log.info("spend by scheduler");
            } catch (TimeoutException e) {
                break;
            }
        }
    }
}
