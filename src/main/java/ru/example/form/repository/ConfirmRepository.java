package ru.example.form.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.example.form.entity.Confirm;

import java.util.List;

public interface ConfirmRepository extends JpaRepository<Confirm, Integer> {
    List<Confirm> findAll();
}
