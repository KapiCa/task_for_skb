package ru.example.form.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.example.form.entity.RegistrationForm;


public interface RegistrationRepository extends JpaRepository<RegistrationForm, Integer> {
    RegistrationForm findByEmail(String email);
}
