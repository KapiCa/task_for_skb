package ru.example.form.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ru.example.form.json.RegistrationRequest;
import ru.example.form.service.dto.RegistrationFormDTO;

import static org.apache.commons.codec.digest.DigestUtils.md5Hex;
@Component
public class RegistrationRequestConverter implements Converter<RegistrationRequest, RegistrationFormDTO> {
    @Override
    public RegistrationFormDTO convert(RegistrationRequest request) {
        return new RegistrationFormDTO()
                .setEmail(request.getEmail())
                .setLogin(request.getLogin())
                .setName(request.getName())
                .setPassword(md5Hex(request.getPassword()));
    }
}
