package ru.example.form.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.example.form.converter.RegistrationRequestConverter;
import ru.example.form.json.RegistrationRequest;
import ru.example.form.service.RegistrationFormService;
import ru.example.form.service.dto.RegistrationAnswerEnum;

import javax.validation.Valid;
import java.util.concurrent.TimeoutException;

import static ru.example.form.service.dto.RegistrationAnswerEnum.*;

@RestController
@RequiredArgsConstructor
public class RegistrationController {
    private final RegistrationFormService registrationFormService;
    private final RegistrationRequestConverter registrationRequestConverter;

    @PostMapping(value = "/registration", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<?> registration(@RequestBody @Valid RegistrationRequest request) throws TimeoutException {

        RegistrationAnswerEnum answerEnum = registrationFormService.regist(registrationRequestConverter.convert(request));
        if (answerEnum == ALREADY) {
            return new ResponseEntity<>("Already exist", HttpStatus.BAD_REQUEST);
        }  else if (answerEnum == WAIT) {
            return new ResponseEntity<>("Please, wait for confirm", HttpStatus.OK);
        }  else  if (answerEnum == CANCEL) {
            return new ResponseEntity<>("Your data is not correct, please try again", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Well done, check your mail", HttpStatus.OK);
        }
    }
}
