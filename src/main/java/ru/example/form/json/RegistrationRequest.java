package ru.example.form.json;


import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;


@Data
public class RegistrationRequest {
    @NotNull
    @Email
    private String email;
    @NotNull
    private String login;
    @NotNull
    private String password;
    @NotNull
    private String name;

}
