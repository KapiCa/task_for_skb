package ru.example.form.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;

@Entity
@Table(name = "registration_for_confirm")
@Data
@Accessors(chain = true)
public class Confirm {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @OneToOne
    @JoinColumn(name = "login_form_id")
    private RegistrationForm registrationForm;
}
